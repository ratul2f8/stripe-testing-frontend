Install node: 16.13.0(tested)

Install yarn:

`npm i -g yarn`

Install dependencies

`yarn install --frozen-lockfile`

Start the frontend server

`yarn start`