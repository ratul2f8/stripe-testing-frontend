import React, { ReactNode } from "react";

interface IProps {
  children?: React.ReactChild | React.ReactChild[];
}

const LayoutComponent: React.FC<IProps> = ({ children }) => {
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        width: "100vw",
        height: "100vh",
        overflow: "auto",
        flexDirection: "column",
      }}>
      {children}
    </div>
  );
};

export default LayoutComponent;
