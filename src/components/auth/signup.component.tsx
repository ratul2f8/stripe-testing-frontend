import { Button, Form, Input } from "antd";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { selectAuthInfo, signUpThunk } from "../../features/auth.slice";

const SignUpComponent: React.FC = () => {
  const onFinish = (e: { email: string; password: string; name: string }) => {
    const { email, password, name } = e;
    dispatch(signUpThunk({ email, password, name }));
  };
  const onFinishFailed = () => {
    console.error("Something error happened");
  };
  const authInfo = useSelector(selectAuthInfo);
  const dispatch = useDispatch();
  return (
    <Form
      name="basic"
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 20 }}
      initialValues={{ remember: true }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
      style={{ width: "80vw", maxWidth: "500px" }}>
      <Form.Item
        label="Name"
        name="name"
        rules={[
          { required: true, message: "Please input your name!" },
          { min: 3, message: "Name must be at least 3 characters long" },
        ]}>
        <Input />
      </Form.Item>
      <Form.Item
        label="Email"
        name="email"
        rules={[{ required: true, message: "Please input your email!" }]}>
        <Input type={"email"} />
      </Form.Item>

      <Form.Item
        label="Password"
        name="password"
        rules={[
          { required: true, message: "Please input your password!" },
          { min: 6, message: "Password must be of at least 6 characters long" },
        ]}>
        <Input.Password />
      </Form.Item>

      <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
        <Button type="primary" htmlType="submit" disabled={authInfo.inProgress}>
          Sign Up
        </Button>
      </Form.Item>
    </Form>
  );
};

export default SignUpComponent;
