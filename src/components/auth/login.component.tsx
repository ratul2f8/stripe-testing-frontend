import { Button, Form, Input } from "antd";
import React from "react";
import { useDispatch } from "react-redux";
import { loginThunk } from "../../features/auth.slice";

const LoginComponent: React.FC = () => {
  const onFinish = (e: { email: string; password: string }) => {
    const { email, password } = e;
    dispatch(loginThunk({ email, password }));
  };
  const onFinishFailed = () => {
    console.error("Something error happened");
  };
  const dispatch = useDispatch();
  return (
    <Form
      name="basic"
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 20 }}
      initialValues={{ remember: true }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
      style={{ width: "80vw", maxWidth: "400px" }}>
      <Form.Item
        label="Email"
        name="email"
        rules={[{ required: true, message: "Please input your email!" }]}>
        <Input type={"email"} />
      </Form.Item>

      <Form.Item
        label="Password"
        name="password"
        rules={[
          { required: true, message: "Please input your password!" },
          { min: 6, message: "Password must be of at least 6 characters long" },
        ]}>
        <Input.Password />
      </Form.Item>

      <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
        <Button type="primary" htmlType="submit">
          Login
        </Button>
      </Form.Item>
    </Form>
  );
};

export default LoginComponent;
