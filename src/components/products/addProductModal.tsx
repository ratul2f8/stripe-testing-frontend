import { Button, Form, Input, Modal, Select } from "antd";
import { AxiosError } from "axios";
import { set } from "immer/dist/internal";
import React from "react";
import { axiosInstance } from "../../helpers/getAxiosInstance";

interface IComponentProps {
  toggle: React.Dispatch<React.SetStateAction<boolean>>;
  detectChanges: React.Dispatch<React.SetStateAction<boolean>>;
}
const AddProductsModal: React.FC<IComponentProps> = ({
  toggle,
  detectChanges,
}) => {
  const { Option } = Select;

  const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
  };
  const tailLayout = {
    wrapperCol: { offset: 8, span: 16 },
  };

  const [form] = Form.useForm();
  const [adding, setAdding] = React.useState(false);
  const [changeDetected, setChangeDetected] = React.useState(false);
  const onFinish = (values: any) => {
    setAdding(true);
    axiosInstance
      .post(
        "/v1/products",
        {
          name: values.name,
          categoryId: values.category,
          unitPrice: values.unitPrice,
        },
        {
          withCredentials: true,
        }
      )
      .then(() => {
        setChangeDetected(true);
        form.resetFields();
        setAdding(false);
      })
      .catch((e: AxiosError) => {
        setAdding(false);
        console.error(e);
      });
  };
  const onReset = () => {
    form.resetFields();
  };
  const [categories, setCategories] = React.useState<
    { name: string; id: string }[]
  >([]);
  React.useEffect(() => {
    axiosInstance
      .get("/v1/categories")
      .then((response) => {
        setCategories(response.data?.data ?? []);
      })
      .catch((e) => {
        console.error("Categories parsing error: ", e);
      });
  }, []);
  return (
    <Modal
      visible={true}
      title="Add Product"
      okButtonProps={{ title: "Done" }}
      onCancel={() => {
        if (changeDetected) {
          detectChanges(true);
          toggle(false);
        } else {
          toggle(false);
        }
      }}
      onOk={() => {
        if (changeDetected) {
          detectChanges(true);
          toggle(false);
        } else {
          toggle(false);
        }
      }}>
      <Form {...layout} form={form} name="control-hooks" onFinish={onFinish}>
        <Form.Item
          name="name"
          label="Name"
          rules={[{ required: true, min: 2 }]}>
          <Input />
        </Form.Item>
        <Form.Item
          name="unitPrice"
          label="Unit Price($)"
          rules={[{ required: true }]}>
          <Input type="number" />
        </Form.Item>
        <Form.Item
          name="category"
          label="Category"
          rules={[{ required: true }]}>
          <Select placeholder="Select a category" allowClear>
            {categories.map((obj) => (
              <Option value={obj.id}>{obj.name}</Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item {...tailLayout}>
          <Button type="primary" htmlType="submit" loading={adding}>
            Add Product
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
};
export default AddProductsModal;
