import { Button, Table } from "antd";
import React from "react";
import { DeleteFilled, PlusCircleFilled } from "@ant-design/icons";
import { useDispatch, useSelector } from "react-redux";
import { logoutThunk, selectAuthInfo } from "../../features/auth.slice";
import {
  EachProduct,
  fetchThunk,
  selectProducts,
} from "../../features/productSlice";
import { Link } from "react-router-dom";
import AddProductsModal from "./addProductModal";
import axios from "axios";
import { axiosInstance } from "../../helpers/getAxiosInstance";

const columns = [
  {
    title: "Name",
    dataIndex: "name",
    render: (text: string) => <a>{text}</a>,
  },
  {
    title: "Category",
    dataIndex: ["category", "name"],
  },
  {
    title: "Created By",
    dataIndex: ["user", "name"],
  },
  {
    title: "Price",
    dataIndex: "unitPrice",
  },
  {
    title: "Currency",
    dataIndex: "currency",
  },
];

const ProductsTable: React.FC = () => {
  const currentUserInfo = useSelector(selectAuthInfo);
  const porductsInfo = useSelector(selectProducts);
  const dispatch = useDispatch();
  const [modalOpen, setModalOpen] = React.useState(false);
  React.useEffect(() => {
    dispatch(fetchThunk(porductsInfo.currentPage + 1));
  }, []);
  const rowSelection = {
    onChange: (selectedRowKeys: React.Key[], selectedRows: EachProduct[]) => {
      console.log(
        `selectedRowKeys: ${selectedRowKeys}`,
        "selectedRows: ",
        selectedRows
      );
      setCurrentlySelected(selectedRows);
    },
    getCheckboxProps: (record: EachProduct) => ({
      disabled: record.user.id !== currentUserInfo.userId,
      name: record.name,
    }),
  };

  const [currentlySelected, setCurrentlySelected] = React.useState<
    EachProduct[]
  >([]);
  const [productUpdated, setProductUpdated] = React.useState(false);
  React.useEffect(() => {
    if (productUpdated) {
      dispatch(fetchThunk(1));
      setProductUpdated(false);
    }
  }, [productUpdated, dispatch]);
  const handleRemoval = () => {
    axiosInstance
      .put(
        "/v1/products",
        {
          deleteIds: currentlySelected.map((obj) => obj.id),
        },
        {
          withCredentials: true,
        }
      )
      .then((res) => {
        setProductUpdated(true);
        setCurrentlySelected([]);
      })
      .catch((e) => console.error(e));
  };
  const authInfo = useSelector(selectAuthInfo);
  return (
    <div style={{ width: "95%", height: "auto" }}>
      {modalOpen && (
        <AddProductsModal
          toggle={setModalOpen}
          detectChanges={setProductUpdated}
        />
      )}
      {authInfo.isAuthenticated ? (
        <Button
          style={{ marginBottom: "1em" }}
          type="primary"
          onClick={() => dispatch(logoutThunk())}>
          Logout
        </Button>
      ) : (
        <Link to="/auth">
          <Button style={{ marginBottom: "1em" }} type="primary">
            Login
          </Button>
        </Link>
      )}
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
          width: "100%",
          marginBottom: "1em",
        }}>
        <Button
          icon={<DeleteFilled />}
          type="primary"
          disabled={!authInfo.isAuthenticated || currentlySelected.length === 0}
          onClick={handleRemoval}>
          {" "}
          Remove {currentlySelected.length} items(s)
        </Button>
        <Button
          icon={<PlusCircleFilled />}
          type="primary"
          onClick={() => setModalOpen(true)}
          disabled={!authInfo.isAuthenticated}>
          Add Product
        </Button>
      </div>
      <Table
        rowSelection={{
          type: "checkbox",
          ...rowSelection,
        }}
        columns={columns}
        dataSource={porductsInfo.products.map((obj) => ({
          ...obj,
          key: obj.id,
        }))}
        pagination={{
          defaultCurrent: porductsInfo.currentPage + 1,
          total: porductsInfo.total,
          pageSize: 5,
          onChange: (page) => dispatch(fetchThunk(page)),
        }}
      />
    </div>
  );
};

export default ProductsTable;
