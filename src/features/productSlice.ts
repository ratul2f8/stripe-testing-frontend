import { createSlice, Dispatch, PayloadAction } from "@reduxjs/toolkit";
import { AxiosResponse } from "axios";
import { stat } from "fs";
import { AppThunk, RootState } from "../app/store";
import { axiosInstance } from "../helpers/getAxiosInstance";

export interface EachProduct {
  name: string;
  id: string;
  email: string;
  unitPrice: string;
  currency: string;
  category: {
    name: string;
    id: string;
  };
  user: {
    name: string;
    id: string;
    email: string;
  };
}

interface IInitialState {
  inProgress: boolean;
  error: string | null;
  products: EachProduct[];
  total: number;
  currentPage: number;
}

const initialState: IInitialState = {
  products: [],
  inProgress: false,
  error: null,
  total: 0,
  currentPage: 0,
};

export const productsSlice = createSlice({
  name: "products",
  initialState,
  reducers: {
    fetchInProgress: (state) => {
      state.inProgress = true;
      state.error = null;
      state.products = [];
      state.total = 0;
    },
    fetchCompleted: (
      state,
      action: PayloadAction<{
        data: EachProduct[];
        total: number;
        currentpage: number;
      }>
    ) => {
      const { data, total } = action.payload;
      state.inProgress = false;
      state.error = null;
      state.total = total;
      state.products = data;
    },
    fetchFailed: (state, action: PayloadAction<string>) => {
      state.inProgress = true;
      state.error = action.payload;
      state.products = [];
      state.total = 0;
    },
  },
});

const { fetchInProgress, fetchCompleted, fetchFailed } = productsSlice.actions;

export const selectProducts = (state: RootState) => state.products;

export const fetchThunk =
  (page: number): AppThunk =>
  (dispatch: Dispatch) => {
    dispatch(fetchInProgress());
    axiosInstance
      .get(`/v1/products?page=${page}`)
      .then((response: AxiosResponse<any>) => {
        const { data, page, count } = response.data?.data ?? {};
        dispatch(
          fetchCompleted({
            data: data ?? [],
            currentpage: page ?? 0,
            total: count ?? 0,
          })
        );
      })
      .catch((e) => dispatch(fetchFailed("Something went wrong")));
  };

export default productsSlice.reducer;
