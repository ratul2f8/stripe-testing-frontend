import { createSlice, Dispatch, PayloadAction } from "@reduxjs/toolkit";
import { AppThunk, RootState } from "../app/store";
import { axiosInstance } from "../helpers/getAxiosInstance";

interface IInitialState {
  isAuthenticated: boolean;
  email: string | null;
  name: string | null;
  inProgress: boolean;
  error: string | null;
  userId: string | null;
}

const initialState: IInitialState = {
  isAuthenticated: false,
  email: null,
  name: null,
  inProgress: false,
  error: null,
  userId: null,
};

export const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    authInProgress: (state) => {
      state.email = null;
      state.inProgress = true;
      state.error = null;
      state.isAuthenticated = false;
      state.name = null;
      state.userId = null;
    },
    authCompleted: (
      state,
      action: PayloadAction<{ name: string; email: string; id: string }>
    ) => {
      const { email, name, id } = action.payload;
      state.email = null;
      state.inProgress = false;
      state.error = null;
      state.isAuthenticated = true;
      state.name = name;
      state.email = email;
      state.userId = id;
    },
    authFailed: (state, action: PayloadAction<string>) => {
      state.email = null;
      state.inProgress = false;
      state.error = action.payload;
      state.isAuthenticated = false;
      state.name = null;
      state.userId = null;
    },
    logout: (state) => {
      state.email = null;
      state.inProgress = false;
      state.error = null;
      state.isAuthenticated = false;
      state.name = null;
      state.userId = null;
    },
  },
});

const { authCompleted, authFailed, authInProgress, logout } = authSlice.actions;

export const selectAuthInfo = (state: RootState) => state.auth;

export const loginThunk =
  (payload: { email: string; password: string }): AppThunk =>
  (dispatch: Dispatch) => {
    dispatch(authInProgress());
    axiosInstance
      .post(
        "/v1/user/login",
        {
          ...payload,
        },
        { withCredentials: true }
      )
      .then((response) => {
        dispatch(authCompleted({ ...response.data }));
      })
      .catch((e) => dispatch(authFailed("Something went wrong")));
  };

export const signUpThunk =
  (payload: { email: string; password: string; name: string }): AppThunk =>
  (dispatch: Dispatch) => {
    dispatch(authInProgress());
    axiosInstance
      .post(
        "/v1/user/signup",
        {
          ...payload,
        },
        { withCredentials: true }
      )
      .then((response) => {
        dispatch(authCompleted({ ...response.data }));
      })
      .catch((e) => dispatch(authFailed("Something went wrong")));
  };

export const logoutThunk = (): AppThunk => (dispatch: Dispatch) => {
  dispatch(authInProgress());
  axiosInstance
    .post("/v1/user/logout", null, { withCredentials: true })
    .then(() => {
      dispatch(logout());
    })
    .catch((e) => dispatch(authFailed("Something went wrong")));
};

export default authSlice.reducer;
