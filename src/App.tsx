import { useSelector } from "react-redux";
import { Navigate, Route, Routes } from "react-router-dom";
import "./App.css";
import { selectAuthInfo } from "./features/auth.slice";
import AuthPage from "./pages/auth";
import ProductsPage from "./pages/products";
function App() {
  const authInfo = useSelector(selectAuthInfo);
  return (
    <Routes>
      <Route path="/" element={<Navigate to={"/products"} />} />
      <Route path="/products" element={<ProductsPage />} />
      <Route
        path="/auth"
        element={
          authInfo.isAuthenticated ? (
            <Navigate to={"/products"} />
          ) : (
            <AuthPage />
          )
        }
      />
    </Routes>
  );
}

export default App;
