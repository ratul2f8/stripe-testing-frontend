import { Tabs } from "antd";
import React from "react";
import LoginComponent from "../components/auth/login.component";
import SignUpComponent from "../components/auth/signup.component";
import LayoutComponent from "../components/Layout";

const AuthPage: React.FC = () => {
  const { TabPane } = Tabs;
  return (
    <LayoutComponent>
      <Tabs defaultActiveKey="1">
        <TabPane tab="Login" key="1">
          <LoginComponent />
        </TabPane>
        <TabPane tab="Signup" key="2">
          <SignUpComponent />
        </TabPane>
      </Tabs>
    </LayoutComponent>
  );
};

export default AuthPage;
