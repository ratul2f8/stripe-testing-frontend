import React from "react";
import LayoutComponent from "../components/Layout";
import ProductsTable from "../components/products/table.component";

const ProductsPage: React.FC = () => {
  return (
    <LayoutComponent>
      <ProductsTable />
    </LayoutComponent>
  );
};

export default ProductsPage;
